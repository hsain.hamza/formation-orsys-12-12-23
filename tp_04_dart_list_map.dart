void main() {
  
  // TODO : créer une Map qui contient comme clé le nom des utilisateur et en valeur une list de String qui represente les badges de l'utilisateur

  // initialiser la Map avec des données de test 
  Map<K,V> userBadges; 

  // TODO 2 : Creer une fonction qui ajoute un badge à un utilisateur 

  void addBadgeToUser(String username, String badgeId){
    // TODO 
  }

  // TODO 3 Créer une fonction qui supprime un badge à un utilisateur 

  void removeIfExistsBadgeFromUser(String username, String badgeId){
    // TODO 
  }

  // TODO 4 : Créer une fonction qui affiche les badges d'un utilisateur
  void displayUserBadges(String username){
    //TODO 
  }


  // TODO 5 : Utiliser les fonctions ci dessus pour ajouter/supprimer/afficher les badges des utilisateurs

}
// ################################################################################################## 
import 'dart:core';
import 'dart:math';

const chars = "abcdefghijklmnopqrstuvwxyz0123456789";
// https://siongui.github.io/2017/01/22/dartlang-generate-random-string/
String randomString(int strlen) {
  Random rnd = new Random(new DateTime.now().millisecondsSinceEpoch);
  String result = "";
  for (var i = 0; i < strlen; i++) {
    result += chars[rnd.nextInt(chars.length)];
  }
  return result;
}

List<String> retrieveNetworkResearch(String str){
  return [for(var i = 0 ; i<10 ; i ++) '$str${randomString(10 + i)}'];
}

List<String> retrieveResults(String str){
  // TODO : Ajouter une couche de mise en cache et d'indexation des recherches et n'utiliser retrieveNetworkResearch seulement si la premiere recherche pour le mot passé en parametre (str)
  return retrieveNetworkResearch(str);
}
void main() {
  String searchKey = 'Test1';
  print('LE RESULTAT DE RECHERCHE POUR LE MOTIF $searchKey');
  print(retrieveResults(searchKey));
}
