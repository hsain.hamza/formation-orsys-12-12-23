import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:my_awesome_app/application.dart';
import 'package:my_awesome_app/core/di/locator.dart';
import 'package:my_awesome_app/core/http/http_client.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sembast/sembast.dart';
import 'package:sembast/sembast_io.dart';

Future<void> _initDatabase() async {
  const dbFileName = "my-app.db";
  final appDir = await getApplicationDocumentsDirectory();
  final pathtoDb = "${appDir.path}/$dbFileName";
  final database = await databaseFactoryIo.openDatabase(pathtoDb);
  injector.registerSingleton<Database>(database);
}

_setupHttpClient(){
  HttpClient.client = Dio(BaseOptions(
    baseUrl: "https://demo0635484.mockable.io/api",
    connectTimeout: const Duration(seconds: 10),
  ));
}
Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  _setupHttpClient();

  setupInjector();

  await _initDatabase();

  runApp(const MyApp());
}

