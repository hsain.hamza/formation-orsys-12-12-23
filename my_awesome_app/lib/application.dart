import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_awesome_app/app/constants/routes.dart';
import 'package:my_awesome_app/app/modules/login/domain/bloc/login_bloc.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<LoginBloc>(
      create: (context) => LoginBloc(),
      child: MaterialApp(
        title: 'Flutter',
        theme: ThemeData(
          useMaterial3: true,
        ),
        routes: kRoutes,
        initialRoute: kSplashRoute,
      ),
    );
  }
}
