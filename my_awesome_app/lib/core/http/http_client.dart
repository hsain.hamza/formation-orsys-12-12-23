import 'package:dio/dio.dart';

class HttpClient{

  HttpClient._();

  Dio _client = Dio();

  static final HttpClient _instance = HttpClient._();

  static Dio get client => _instance._client;

  static set client(Dio dio){
    _instance._client = dio;
  }

}