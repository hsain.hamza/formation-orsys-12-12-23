import 'package:get_it/get_it.dart';
import 'package:my_awesome_app/app/modules/login/data/local/providers/login_local_provider.dart';
import 'package:my_awesome_app/app/modules/login/data/remote/providers/login_remote_provider.dart';
import 'package:my_awesome_app/app/modules/login/data/repository/login_repository.dart';
import 'package:my_awesome_app/app/modules/products/data/local/providers/products_local_provider.dart';
import 'package:my_awesome_app/app/modules/products/data/remote/providers/products_provider.dart';
import 'package:my_awesome_app/app/modules/products/data/repository/products_repository.dart';
import 'package:my_awesome_app/app/modules/promotion/data/local/providers/promotion_local_provider.dart';
import 'package:my_awesome_app/app/modules/promotion/data/remote/providers/promotion_provider.dart';
import 'package:my_awesome_app/app/modules/promotion/data/repository/promotion_repository.dart';

GetIt injector = GetIt.instance;

void setupInjector(){
  injector.registerLazySingleton<PromotionProvider>(() => PromotionRestApiProvider());
  injector.registerLazySingleton<PromotionLocalProvider>(() => PromotionDatabaseProvider());
  injector.registerLazySingleton<PromotionRepository>(() => PromotionRepositoryImpl());

  injector.registerLazySingleton<ProductsProvider>(() => ProductsRestApiProvider());
  injector.registerLazySingleton<ProductsLocalProvider>(() => ProductsDatabaseProvider());
  injector.registerLazySingleton<ProductsRepository>(() => ProductsRepositoryImpl());


  injector.registerLazySingleton<LoginRemoteProvider>(() => LoginRemoteProviderImpl());
  injector.registerLazySingleton<LoginLocalProvider>(() => LoginDatabaseProvider());
  injector.registerLazySingleton<LoginRepository>(() => LoginRepositoryImpl());



}
