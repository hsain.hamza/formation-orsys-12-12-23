import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';

class ItemWidget extends StatefulWidget {
  const ItemWidget({Key? key}) : super(key: key);

  @override
  State<ItemWidget> createState() => _ItemWidgetState();
}

class _ItemWidgetState extends State<ItemWidget> {

  int counter = 0;

  @override
  void initState() {
    counter = 100;
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print("REBUILD DU WIDGET");
    return SizedBox(
      width: double.infinity,
      height: 70,
      child: Row(
        children: [
          const Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: 20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Mon super titre",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text("Mon super sous titre"),
                ],
              ),
            ),
          ),

          IconButton(
            onPressed: () {
              setState(() {
                counter  ++ ;
              });

              print("Hello $counter");
            },
            icon: const Icon(
              Icons.favorite,
              color: Colors.red,
              size: 30,
            ),
          ),
           Center(
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: Text("$counter"),
            ),
          ),
        ],
      ),
    );
  }
}
