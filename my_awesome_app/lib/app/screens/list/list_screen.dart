import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_awesome_app/app/modules/promotion/domain/bloc/promotion_bloc.dart';
import 'package:my_awesome_app/app/screens/list/widgets/item_widget.dart';

class ListScreen extends StatefulWidget {
  const ListScreen({Key? key}) : super(key: key);

  @override
  State<ListScreen> createState() => _ListScreenState();
}

class _ListScreenState extends State<ListScreen> {
  final TextEditingController _lastNameController = TextEditingController();
  final TextEditingController _firstNameController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text("Ma page"),
      ),
      body: SizedBox(
        width: double.infinity,
        height: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Form(
                key: _formKey,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                child: Column(
                  children: [
                    TextFormField(
                      controller: _lastNameController,
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                        hintText: "Ici last name",
                        labelText: "Last Name",
                      ),
                      validator: (value){
                        if(value!= null && value.isEmpty){
                          return "Ce champ est requis";
                        }
                        return null;
                      },
                    ),
                    TextFormField(
                      controller: _firstNameController,
                    ),
                  ],
                ),
              ),
            ),
            ElevatedButton(
              onPressed: () {
                if(_formKey.currentState!.validate()){
                  print(_lastNameController.text);
                  print(_firstNameController.text);
                  _firstNameController.text = "MA NOUVELLE VALEUR";

                }
              },
              child: Text("SEND FORM"),
            ),
            BlocConsumer<PromotionBloc, PromotionState>(
              builder: (context, state) {
                if (state is PromotionRefreshInProgress) {
                  return const CircularProgressIndicator();
                }
                return Text("$state");
              },
              listener: (context, state) {
                if (state is PromotionRefreshed) {
                  const snackBar = SnackBar(
                    content: Text('Yay! Promotion refreshed!'),
                  );
                  ScaffoldMessenger.of(context).showSnackBar(snackBar);
                }
              },
            ),
            CachedNetworkImage(
              imageUrl: "http://via.placeholder.com/100x100",
              placeholder: (context, url) => const CircularProgressIndicator(),
              errorWidget: (context, url, error) => const Icon(Icons.error),
            ),
            SizedBox(
              height: 200,
              child: Image.asset("assets/images/splash/plant.png"),
            ),
            SizedBox(
              height: 150,
              width: double.infinity,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: [
                  for (var i in [1, 2, 3, 4, 5, 6])
                    Container(
                      margin: EdgeInsets.only(right: 20),
                      width: 150,
                      height: 150,
                      color: Colors.red,
                    ),
                ],
              ),
            ),
            Expanded(
              child: ListView.builder(
                itemCount: 1000,
                itemBuilder: (context, index) {
                  return const ItemWidget();
                },
              ),
            ),
            if (true) ...[
              const RotatedBox(
                quarterTurns: 3,
                child: Text("HELLO"),
              ),
              const RotatedBox(
                quarterTurns: 3,
                child: Text("HELLO"),
              ),
            ]
          ],
        ),
      ),
    );
  }
}
