import 'dart:io';

import 'package:flutter/material.dart';
import 'package:my_awesome_app/app/constants/routes.dart';

class SplashFooter extends StatelessWidget {
  const SplashFooter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const SizedBox(
      height: 170,
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
           Text(
            "Worldwide delivery\nwithin 10 - 15 days",
            style: TextStyle(
              fontSize: 20,
            ),
          ),

        ],
      ),
    );
  }
}
