import 'package:flutter/material.dart';

class SplashHeader extends StatelessWidget {
  const SplashHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return const SizedBox(
      height: 130,
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          RotatedBox(
            quarterTurns: 3,
            child: Text("Planto.Shop"),
          ),
          VerticalDivider(
            width: 20,
            thickness: 2,
          ),
          Text(
            "Plant a\ntree for\nlife",
            style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.bold,
              height: 1
            ),

          )
        ],
      ),
    );
  }
}
