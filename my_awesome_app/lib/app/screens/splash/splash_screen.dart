import 'dart:async';

import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_awesome_app/app/constants/routes.dart';
import 'package:my_awesome_app/app/modules/login/domain/bloc/login_bloc.dart';
import 'package:my_awesome_app/app/screens/splash/widgets/splash_header.dart';

import 'widgets/splash_footer.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> with AfterLayoutMixin<SplashScreen> {

  @override
  FutureOr<void> afterFirstLayout(BuildContext context) {
    Future.delayed(const Duration(seconds: 3)).then((_){
      BlocProvider.of<LoginBloc>(context).add(CheckLogin());
    });
  }
  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        if(state is Authenticated){
          Navigator.pushNamedAndRemoveUntil(context, kHomeRoute, (route) => false);
        } else if(state is Unauthenticated){
          Navigator.pushNamedAndRemoveUntil(context, kLoginRoute, (route) => false);
        }
      },
      child: Scaffold(
        body: SafeArea(
          child: Column(
            children: [
              const SplashHeader(),
              Expanded(
                child: Image.asset("assets/images/splash/plant.png"),
              ),
              const SplashFooter(),
            ],
          ),
        ),
      ),
    );
  }


}
