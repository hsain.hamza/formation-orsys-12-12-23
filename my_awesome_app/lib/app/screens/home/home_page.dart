import 'dart:async';

import 'package:after_layout/after_layout.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_awesome_app/app/constants/routes.dart';
import 'package:my_awesome_app/app/modules/login/domain/bloc/login_bloc.dart';
import 'package:my_awesome_app/app/modules/products/data/remote/models/products_models.dart';
import 'package:my_awesome_app/app/modules/products/data/remote/providers/products_provider.dart';
import 'package:my_awesome_app/app/modules/products/data/repository/products_repository.dart';
import 'package:my_awesome_app/app/modules/products/domain/bloc/products_bloc.dart';
import 'package:my_awesome_app/app/modules/promotion/data/remote/models/promotion_models.dart';
import 'package:my_awesome_app/app/modules/promotion/data/remote/providers/promotion_provider.dart';
import 'package:my_awesome_app/app/modules/promotion/data/repository/promotion_repository.dart';
import 'package:my_awesome_app/app/modules/promotion/domain/bloc/promotion_bloc.dart';
import 'package:my_awesome_app/app/screens/home/widgets/home_header.dart';
import 'package:my_awesome_app/app/screens/home/widgets/product_item.dart';
import 'package:my_awesome_app/app/screens/home/widgets/products_section.dart';
import 'package:my_awesome_app/app/screens/home/widgets/promo_banner.dart';
import 'package:my_awesome_app/app/screens/home/widgets/promotion_widget.dart';
import 'package:my_awesome_app/core/di/locator.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with AfterLayoutMixin<HomePage> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  FutureOr<void> afterFirstLayout(BuildContext context) {
    _refreshData(context);
  }

  _refreshData(BuildContext context) {
    BlocProvider.of<PromotionBloc>(context).add(RefreshPromotion());
    BlocProvider.of<ProductsBloc>(context).add(FetchProducts());
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        if(state is Unauthenticated){
          Navigator.pushNamedAndRemoveUntil(context, kLoginRoute, (route) => false);
        }
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: const Text("Home"),
          actions: [
            IconButton(
              onPressed: () {
                BlocProvider.of<LoginBloc>(context).add(LogoutUser());
              },
              icon: Icon(Icons.logout),
            ),
          ],
        ),
        body: SafeArea(
          child: SizedBox(
            width: double.infinity,
            height: double.infinity,
            child: RefreshIndicator(
              onRefresh: () async {
                _refreshData(context);
              },
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(
                      height: 20,
                    ),
                    const HomeHeader(),

                    const PromotionWidget(),

                    Container(
                      width: double.infinity,
                      height: 60,
                      color: Colors.green,
                    ),

                    const Padding(
                      padding: EdgeInsets.all(14.0),
                      child: Text(
                        "Les produits à la une",
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),

                    const ProductsSection(),

                    const PromotionWidget(),

                    const Padding(
                      padding: EdgeInsets.all(14.0),
                      child: Text(
                        "Autres produits",
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    // TODO
                    const ProductsSection(),
                    const PromotionWidget(),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
