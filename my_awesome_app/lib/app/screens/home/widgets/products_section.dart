import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_awesome_app/app/modules/products/domain/bloc/products_bloc.dart';
import 'package:my_awesome_app/app/screens/home/widgets/product_item.dart';

class ProductsSection extends StatelessWidget {
  const ProductsSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 230,
      child: BlocBuilder<ProductsBloc,ProductsState>(
        builder: (context, state) {
          if (state is ProductsLoadingError) {
            return const Center(
              child: Icon(Icons.warning_amber),
            );
          }
          if (state is ProductsLoaded) {
            return ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: state.data.length,
              itemBuilder: (c, index) {
                return ProductItemWidget(
                  title: state.data[index].title ?? "",
                  price: state.data[index].price ?? "0",
                  imageUrl: state.data[index].picture ?? "",
                );
              },
            );
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
