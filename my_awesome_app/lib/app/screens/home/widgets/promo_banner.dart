import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class PromoBanner extends StatelessWidget {
  final String title;
  final String subtitle;
  final String imageUrl;

  const PromoBanner({
    super.key,
    required this.title,
    required this.subtitle,
    required this.imageUrl,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        height: 140,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: const Color.fromARGB(255, 209, 235, 192),
        ),
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Row(
            children: [
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      title,
                      style: const TextStyle(
                          fontSize: 36, fontWeight: FontWeight.w600),
                    ),
                    Text(
                      subtitle,
                      style: const TextStyle(
                        fontSize: 16,
                        color: Color.fromARGB(255, 108, 120, 102),
                      ),
                    )
                  ],
                ),
              ),
              CachedNetworkImage(
                imageUrl: imageUrl,
                placeholder: (_, val) => const CircularProgressIndicator(),
                errorWidget: (_,val,error) => const Icon(Icons.warning),
              ),

            ],
          ),
        ),
      ),
    );
  }
}
