import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class ProductItemWidget extends StatelessWidget {
  final String title;
  final String price;
  final String imageUrl;

  const ProductItemWidget({
    Key? key,
    required this.title,
    required this.price,
    required this.imageUrl,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 10),
      width: 180,
      height: 230,
      decoration: const BoxDecoration(
        color: Color(0xffe8e8e8),
        borderRadius: BorderRadius.all(Radius.circular(20)),
      ),
      child: Column(
        children: [
          //PRODUCT IMAGE SECTION
          Expanded(
            child: SizedBox(
              width: double.infinity,
              child: Stack(
                children: [
                  Center(
                    child: CachedNetworkImage(
                      imageUrl: imageUrl,
                      placeholder: (_,__) => const CircularProgressIndicator(),
                      errorWidget: (_,__,___) => const Icon(Icons.warning_amber),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: RotatedBox(
                      quarterTurns: 3,
                      child: Text(title),
                    ),
                  ),
                  Positioned(
                    right: 6,
                    top: 10,
                    child: Text(price),
                  ),
                ],
              ),
            ),
          ),
          // FOOTER SECTION
          Padding(
            padding: const EdgeInsets.all(6),
            child: SizedBox(
              height: 50,
              width: double.infinity,
              child: Row(
                children: [
                  Expanded(
                    child: Container(
                      height: 50,
                      decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(30)),
                      ),
                      child: const Center(child: Text("Add to cart")),
                    ),
                  ),
                  const SizedBox(
                    width: 6,
                  ),
                  Container(
                    decoration: const BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                    ),
                    child: const Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Icon(
                        Icons.favorite_border,
                        color: Colors.white,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
