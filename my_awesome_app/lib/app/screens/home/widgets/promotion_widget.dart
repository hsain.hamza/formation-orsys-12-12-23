import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_awesome_app/app/modules/promotion/domain/bloc/promotion_bloc.dart';
import 'package:my_awesome_app/app/screens/home/widgets/promo_banner.dart';

class PromotionWidget extends StatelessWidget {
  const PromotionWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PromotionBloc, PromotionState>(builder: (_, state) {
      if (state is PromotionRefreshError) {
        return const Text("Une erreur");
      }
      if (state is PromotionRefreshed) {
        return PromoBanner(
          title: state.data.promoMessage ?? "N/A",
          subtitle: state.data.promoSubtitle ?? "N/A",
          imageUrl: state.data.promoImage ?? "",
        );
      }
      return const Center(
        child: CircularProgressIndicator(),
      );
    });
  }
}
