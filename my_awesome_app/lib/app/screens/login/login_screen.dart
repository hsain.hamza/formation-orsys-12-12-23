import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_awesome_app/app/constants/routes.dart';
import 'package:my_awesome_app/app/modules/login/data/repository/login_repository.dart';
import 'package:my_awesome_app/app/modules/login/domain/bloc/login_bloc.dart';
import 'package:my_awesome_app/core/di/locator.dart';

class LoginScreen extends StatelessWidget {
  LoginScreen({Key? key}) : super(key: key);

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        if(state is Authenticated){
          Navigator.of(context).pushNamedAndRemoveUntil(kHomeRoute, (route) => false);
        }
      },
      child: Scaffold(
        body: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset("assets/images/login/user.png"),
                TextFormField(
                  controller: _emailController,
                  keyboardType: TextInputType.emailAddress,
                  decoration: const InputDecoration(
                    hintText: "Votre identifiant",
                    labelText: "Identifiant",
                  ),
                  validator: (value) {
                    if (value != null && value.isEmpty) {
                      return "Ce champ est requis";
                    }
                    return null;
                  },
                ),
                TextFormField(
                  controller: _passwordController,
                  keyboardType: TextInputType.visiblePassword,
                  obscureText: true,
                  decoration: const InputDecoration(
                    labelText: "Mot de passe",
                  ),
                  validator: (value) {
                    if (value != null && value.length < 6) {
                      return "Ce champ doit contenir au minimum 6";
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 20,),
                ElevatedButton(
                  onPressed: () => _validateForm(context),
                  child: const Text("Connexion"),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _validateForm(BuildContext context) {
    if (_formKey.currentState!.validate()) {
      BlocProvider.of<LoginBloc>(context).add(LoginUser(
          username: _emailController.text, password: _passwordController.text));
      print(_passwordController.text);
      print(_emailController.text);
    }
  }
}
