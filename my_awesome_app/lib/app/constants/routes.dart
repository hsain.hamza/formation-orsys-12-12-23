import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_awesome_app/app/modules/products/domain/bloc/products_bloc.dart';
import 'package:my_awesome_app/app/modules/promotion/domain/bloc/promotion_bloc.dart';
import 'package:my_awesome_app/app/screens/home/home_page.dart';
import 'package:my_awesome_app/app/screens/list/list_screen.dart';
import 'package:my_awesome_app/app/screens/login/login_screen.dart';
import 'package:my_awesome_app/app/screens/splash/splash_screen.dart';

const String kHomeRoute = '/home';
const String kSplashRoute = '/splash';
const String kListRoute = '/list';
const String kLoginRoute = '/login';

final kRoutes = {

  kHomeRoute: (_) => MultiBlocProvider(
    providers: [
      BlocProvider<PromotionBloc>(
        create: (_) => PromotionBloc(),
      ),
      BlocProvider<ProductsBloc>(
        create: (_) => ProductsBloc(),
      ),
    ],
    child: const HomePage(),
  ),
  kSplashRoute: (_) => const SplashScreen(),
  kLoginRoute: (_) => LoginScreen(),
  kListRoute: (_) => MultiBlocProvider(
        providers: [
          BlocProvider<PromotionBloc>(
            create: (c) => PromotionBloc(),
          ),
        ],
        child: const ListScreen(),
      ),
};
