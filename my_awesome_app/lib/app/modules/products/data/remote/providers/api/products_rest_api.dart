import 'package:dio/dio.dart';
import 'package:my_awesome_app/app/modules/products/data/remote/models/products_models.dart';
import 'package:retrofit/retrofit.dart';

part 'products_rest_api.g.dart';

@RestApi()
abstract class ProductsRestApi {

  factory ProductsRestApi(Dio dio, {String baseUrl}) = _ProductsRestApi;

  @GET("/products")
  Future<List<ProductResponse>> get();

}