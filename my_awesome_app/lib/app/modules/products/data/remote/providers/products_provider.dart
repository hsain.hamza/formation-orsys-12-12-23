import 'package:dio/dio.dart';
import 'package:my_awesome_app/app/modules/products/data/remote/models/products_models.dart';
import 'package:my_awesome_app/app/modules/products/data/remote/providers/api/products_rest_api.dart';
import 'package:my_awesome_app/core/http/http_client.dart';

abstract class ProductsProvider {
  Future<List<ProductResponse>> retrieve();
}

class ProductsRestApiProvider extends ProductsProvider {
  final _restApi = ProductsRestApi(HttpClient.client);

  @override
  Future<List<ProductResponse>> retrieve() async {
    return _restApi.get();
  }
}
