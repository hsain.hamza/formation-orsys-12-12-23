import 'package:json_annotation/json_annotation.dart';

part 'products_models.g.dart';

@JsonSerializable()
class ProductResponse {
  @JsonKey(name: "product_id")
  String? productId;
  String? picture;
  String? price;
  String? holder;
  String? title;
  String? description;
  @JsonKey(name: "created_at")
  DateTime? createdAt;

  ProductResponse({
    this.productId,
    this.picture,
    this.price,
    this.holder,
    this.title,
    this.description,
    this.createdAt,
  });

  factory ProductResponse.fromJson(Map<String, dynamic> json) => _$ProductResponseFromJson(json);
  Map<String, dynamic> toJson() => _$ProductResponseToJson(this);
}
