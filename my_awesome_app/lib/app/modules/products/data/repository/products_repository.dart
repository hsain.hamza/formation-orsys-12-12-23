

import 'package:my_awesome_app/app/modules/products/data/local/providers/products_local_provider.dart';
import 'package:my_awesome_app/app/modules/products/data/remote/models/products_models.dart';
import 'package:my_awesome_app/app/modules/products/data/remote/providers/products_provider.dart';
import 'package:my_awesome_app/core/di/locator.dart';

abstract class ProductsRepository {

  Future<List<ProductResponse>> retrieveAllProducts();

}

class ProductsRepositoryImpl extends ProductsRepository {

  final ProductsProvider _productsProvider = injector.get();
  final ProductsLocalProvider _productsLocalProvider = injector.get();

  @override
  Future<List<ProductResponse>> retrieveAllProducts() async {
    await _productsLocalProvider.init();
    try{
      final remoteResult = await _productsProvider.retrieve();
      await _productsLocalProvider.store(remoteResult);
      return remoteResult;
    } catch(e){
      return _productsLocalProvider.fetch();
    }
  }

}