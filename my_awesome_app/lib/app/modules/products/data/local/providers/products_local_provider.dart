import 'package:my_awesome_app/app/modules/products/data/remote/models/products_models.dart';
import 'package:my_awesome_app/core/di/locator.dart';
import 'package:sembast/sembast.dart';

abstract class ProductsLocalProvider {
  Future<void> init();

  Future<List<ProductResponse>> fetch();

  Future<void> store(List<ProductResponse> data);

  Future<void> delete();
}

class ProductsDatabaseProvider extends ProductsLocalProvider {
  final _database = injector.get<Database>();
  final _productsStore = intMapStoreFactory.store("products_store");

  @override
  Future<void> delete() async {
    await _productsStore.delete(_database);
  }

  @override
  Future<List<ProductResponse>> fetch() async {
    final dbResults = await _productsStore.find(_database);
    return dbResults.map((e) => ProductResponse.fromJson(e.value)).toList();
  }

  @override
  Future<void> init() async {}

  @override
  Future<void> store(List<ProductResponse> data) async {
    await _database.transaction((transaction) async {
      await _productsStore.delete(transaction);
      for (ProductResponse elem in data) {
        await _productsStore.add(transaction, elem.toJson());
      }
    });
  }
}
