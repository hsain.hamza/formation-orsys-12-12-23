part of 'products_bloc.dart';

@immutable
abstract class ProductsState {}

class ProductsInitial extends ProductsState {}

class ProductsLoading extends ProductsState{}

class ProductsLoaded extends ProductsState {
  final List<ProductResponse> data;

  ProductsLoaded({required this.data});
}

class ProductsLoadingError extends ProductsState{
  final dynamic error;
  ProductsLoadingError({required this.error});

}

