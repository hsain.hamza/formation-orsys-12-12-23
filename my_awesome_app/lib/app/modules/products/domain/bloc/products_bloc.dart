import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:my_awesome_app/app/modules/products/data/remote/models/products_models.dart';
import 'package:my_awesome_app/app/modules/products/data/repository/products_repository.dart';
import 'package:my_awesome_app/core/di/locator.dart';

part 'products_event.dart';
part 'products_state.dart';


class ProductsBloc extends Bloc<ProductsEvent, ProductsState> {
  final ProductsRepository _productsRepository = injector.get();

  ProductsBloc() : super(ProductsInitial()) {
    on<FetchProducts>((event, emit) async {
      emit(ProductsLoading());
      try{
        final response = await _productsRepository.retrieveAllProducts();
        emit(ProductsLoaded(data: response));
      } catch(e){
        emit(ProductsLoadingError(error: e));
      }
    });
  }
}
