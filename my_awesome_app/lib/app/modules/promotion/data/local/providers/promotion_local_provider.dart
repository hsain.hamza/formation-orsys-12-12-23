import 'dart:convert';

import 'package:my_awesome_app/app/modules/promotion/data/remote/models/promotion_models.dart';
import 'package:my_awesome_app/core/di/locator.dart';
import 'package:sembast/sembast.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class PromotionLocalProvider {
  Future<void> init();
  Future<PromotionResponse> fetch();
  Future<void> store(PromotionResponse data);
  Future<void> delete();
}

class PromotionDatabaseProvider extends PromotionLocalProvider {

  final _database = injector.get<Database>();
  final StoreRef _promotionStore = stringMapStoreFactory.store("promo_store");


  @override
  Future<void> store(PromotionResponse data) async {

    await _promotionStore.record("promoData").put(_database, data.toJson());
  }

  @override
  Future<PromotionResponse> fetch() async {
    final dbData = await _promotionStore.record("promoData").get(_database);
    if(dbData != null){
      return PromotionResponse.fromJson(dbData as Map<String, dynamic>);
    }
    throw "404";
  }
  @override
  Future<void> delete() async {
    await _promotionStore.record("promoData").delete(_database);
  }

  @override
  Future<void> init() async {}



}


const String kPromoMessageKey = "PROMO_MESSAGE";
const String kPromoSubtitleKey = "PROMO_SUBTITLE";
const String kPromoImageKey = "PROMO_IMAGE";


class PromotionSharedPrefProvider extends PromotionLocalProvider {
   SharedPreferences? _prefs;

  @override
  Future<void> init() async  {
    _prefs ??= await SharedPreferences.getInstance();
  }

   @override
   Future<void> store(PromotionResponse data) async {

    // final serialized = jsonEncode(data.toJson());
    // stocker
    // PromotionResponse.fromJson(jsonDecode(serialized) as Map<String, dynamic>);

     await _prefs?.setString(kPromoMessageKey, data.promoMessage ?? "");
     await _prefs?.setString(kPromoSubtitleKey, data.promoSubtitle ?? "");
     await _prefs?.setString(kPromoImageKey, data.promoImage ?? "");
   }

   @override
   Future<PromotionResponse> fetch() async {
     return PromotionResponse(
       promoImage: _prefs?.getString(kPromoImageKey),
       promoSubtitle: _prefs?.getString(kPromoSubtitleKey),
       promoMessage: _prefs?.getString(kPromoMessageKey),
     );
   }

  @override
  Future<void> delete() async {
    await _prefs?.remove(kPromoImageKey);
    await _prefs?.remove(kPromoSubtitleKey);
    await _prefs?.remove(kPromoMessageKey);
  }







}