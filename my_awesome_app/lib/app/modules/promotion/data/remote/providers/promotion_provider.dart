import 'package:dio/dio.dart';
import 'package:my_awesome_app/app/modules/promotion/data/remote/models/promotion_models.dart';
import 'package:my_awesome_app/app/modules/promotion/data/remote/providers/api/promotion_rest_api.dart';
import 'package:my_awesome_app/core/http/http_client.dart';

abstract class PromotionProvider {
  Future<PromotionResponse> retrieve();
}

class PromotionRestApiProvider extends PromotionProvider{
  final _promoRestApi = PromotionRestApi(HttpClient.client);

  @override
  Future<PromotionResponse> retrieve() async {
      return _promoRestApi.get();
  }
}

