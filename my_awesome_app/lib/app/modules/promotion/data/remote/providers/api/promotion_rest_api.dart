import 'package:dio/dio.dart';
import 'package:my_awesome_app/app/modules/promotion/data/remote/models/promotion_models.dart';
import 'package:retrofit/retrofit.dart';

part 'promotion_rest_api.g.dart';

@RestApi()
abstract class PromotionRestApi {

  factory PromotionRestApi(Dio dio, {String baseUrl}) = _PromotionRestApi;

  @GET("/configuration")
  Future<PromotionResponse> get();

}