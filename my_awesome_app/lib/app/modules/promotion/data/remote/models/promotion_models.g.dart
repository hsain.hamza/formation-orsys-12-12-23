// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'promotion_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PromotionResponse _$PromotionResponseFromJson(Map<String, dynamic> json) =>
    PromotionResponse(
      promoMessage: json['promo_message'] as String?,
      promoSubtitle: json['promo_subtitle'] as String?,
      promoImage: json['image_url'] as String?,
    );

Map<String, dynamic> _$PromotionResponseToJson(PromotionResponse instance) =>
    <String, dynamic>{
      'promo_message': instance.promoMessage,
      'promo_subtitle': instance.promoSubtitle,
      'image_url': instance.promoImage,
    };
