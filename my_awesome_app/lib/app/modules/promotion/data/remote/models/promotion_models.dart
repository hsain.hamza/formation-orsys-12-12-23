import 'package:json_annotation/json_annotation.dart';

part 'promotion_models.g.dart';

@JsonSerializable()
class PromotionResponse {
  @JsonKey(name: "promo_message")
  final String? promoMessage;
  @JsonKey(name: "promo_subtitle")
  final String? promoSubtitle;
  @JsonKey(name: "image_url")
  final String? promoImage;

  PromotionResponse({
     this.promoMessage,
     this.promoSubtitle,
     this.promoImage,
  });

  factory PromotionResponse.fromJson(Map<String, dynamic> json) {
    return _$PromotionResponseFromJson(json);
  }

  Map<String, dynamic> toJson() => _$PromotionResponseToJson(this);
}
