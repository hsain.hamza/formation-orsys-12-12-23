import 'package:my_awesome_app/app/modules/promotion/data/local/providers/promotion_local_provider.dart';
import 'package:my_awesome_app/app/modules/promotion/data/remote/models/promotion_models.dart';
import 'package:my_awesome_app/app/modules/promotion/data/remote/providers/promotion_provider.dart';
import 'package:my_awesome_app/core/di/locator.dart';

enum CacheStrategy { cacheOnly, networkOnly, networkOrCache }

abstract class PromotionRepository {
  Future<PromotionResponse> retrieveLastPromotion();
}

class PromotionRepositoryImpl extends PromotionRepository {
  final PromotionProvider _promotionProvider = injector.get();
  final PromotionLocalProvider _promotionLocalProvider = injector.get();

  @override
  Future<PromotionResponse> retrieveLastPromotion() async {
    await _promotionLocalProvider.init();
    try {
      final networkResponse = await _promotionProvider.retrieve();
      await _promotionLocalProvider.store(networkResponse);
      return networkResponse;
    } catch (e) {
      return _promotionLocalProvider.fetch();
    }
  }
}
