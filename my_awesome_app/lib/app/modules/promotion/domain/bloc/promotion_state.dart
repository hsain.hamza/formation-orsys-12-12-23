part of 'promotion_bloc.dart';

@immutable
abstract class PromotionState {}

class PromotionInitial extends PromotionState {}

class PromotionRefreshed extends PromotionState {
  final PromotionResponse data;

  PromotionRefreshed({required this.data});
}

class PromotionRefreshInProgress extends PromotionState {
}

class PromotionRefreshError extends PromotionState {
  final dynamic error;

  PromotionRefreshError({required this.error});

}
