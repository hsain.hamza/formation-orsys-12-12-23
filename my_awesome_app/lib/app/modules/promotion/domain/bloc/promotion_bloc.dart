import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:my_awesome_app/app/modules/promotion/data/remote/models/promotion_models.dart';
import 'package:my_awesome_app/app/modules/promotion/data/repository/promotion_repository.dart';
import 'package:my_awesome_app/core/di/locator.dart';

part 'promotion_event.dart';
part 'promotion_state.dart';

class PromotionBloc extends Bloc<PromotionEvent, PromotionState> {

  final PromotionRepository _promotionRepository = injector.get();

  PromotionBloc() : super(PromotionInitial()) {

    on<RefreshPromotion>((event, emit) async {
      emit(PromotionRefreshInProgress());
      try{
        final promoData = await _promotionRepository.retrieveLastPromotion();
        print("Je suis bien dans la fonction on<RefreshPromotion>");
        emit(PromotionRefreshed(data: promoData));
      } catch(e){
        emit(PromotionRefreshError(error: e));
      }

    });
  }


}
