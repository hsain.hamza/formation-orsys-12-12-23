import 'package:my_awesome_app/app/modules/login/data/remote/models/login_models.dart';
import 'package:my_awesome_app/core/di/locator.dart';
import 'package:sembast/sembast.dart';

abstract class LoginLocalProvider {
  Future<void> init();
  Future<LoginResponse> fetch();
  Future<void> store(LoginResponse data);
  Future<void> delete();
}

class LoginDatabaseProvider extends LoginLocalProvider {

  final _database = injector.get<Database>();
  final StoreRef _loginStore = stringMapStoreFactory.store("login_store");


  @override
  Future<void> store(LoginResponse data) async {

    await _loginStore.record("login").put(_database, data.toJson());
  }

  @override
  Future<LoginResponse> fetch() async {
    final dbData = await _loginStore.record("login").get(_database);
    if(dbData != null){
      return LoginResponse.fromJson(dbData as Map<String, dynamic>);
    }
    throw "404";
  }
  @override
  Future<void> delete() async {
    await _loginStore.record("login").delete(_database);
  }

  @override
  Future<void> init() async {}

}


