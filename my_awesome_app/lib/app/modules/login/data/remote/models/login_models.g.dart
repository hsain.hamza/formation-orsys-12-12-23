// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginResponse _$LoginResponseFromJson(Map<String, dynamic> json) =>
    LoginResponse(
      userInfo: json['user_infos'] == null
          ? null
          : LoginUserInfos.fromJson(json['user_infos'] as Map<String, dynamic>),
      token: json['token'] == null
          ? null
          : LoginToken.fromJson(json['token'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$LoginResponseToJson(LoginResponse instance) =>
    <String, dynamic>{
      'user_infos': instance.userInfo?.toJson(),
      'token': instance.token?.toJson(),
    };

LoginRequest _$LoginRequestFromJson(Map<String, dynamic> json) => LoginRequest(
      email: json['email'] as String,
      password: json['password'] as String,
    );

Map<String, dynamic> _$LoginRequestToJson(LoginRequest instance) =>
    <String, dynamic>{
      'email': instance.email,
      'password': instance.password,
    };

LoginUserInfos _$LoginUserInfosFromJson(Map<String, dynamic> json) =>
    LoginUserInfos(
      nom: json['nom'] as String?,
      prenom: json['prenom'] as String?,
      email: json['email'] as String?,
      login: json['login'] as String?,
    );

Map<String, dynamic> _$LoginUserInfosToJson(LoginUserInfos instance) =>
    <String, dynamic>{
      'nom': instance.nom,
      'prenom': instance.prenom,
      'email': instance.email,
      'login': instance.login,
    };

LoginToken _$LoginTokenFromJson(Map<String, dynamic> json) => LoginToken(
      accessToken: json['access_token'] as String?,
      expireIn: json['expire_in'] as String?,
      tokenType: json['token_type'] as String?,
    );

Map<String, dynamic> _$LoginTokenToJson(LoginToken instance) =>
    <String, dynamic>{
      'access_token': instance.accessToken,
      'expire_in': instance.expireIn,
      'token_type': instance.tokenType,
    };
