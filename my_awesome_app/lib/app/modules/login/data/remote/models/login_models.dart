import 'package:json_annotation/json_annotation.dart';

part 'login_models.g.dart';

@JsonSerializable(explicitToJson: true)
class LoginResponse {
  @JsonKey(name: "user_infos")
  final LoginUserInfos? userInfo;
  final LoginToken? token;

  LoginResponse({this.userInfo, this.token});

  factory LoginResponse.fromJson(Map<String, dynamic> json) =>
      _$LoginResponseFromJson(json);

  Map<String, dynamic> toJson() => _$LoginResponseToJson(this);
}

@JsonSerializable(explicitToJson: true)
class LoginRequest {
  final String email;
  final String password;

  LoginRequest({
    required this.email,
    required this.password,
  });

  factory LoginRequest.fromJson(Map<String, dynamic> json) =>
      _$LoginRequestFromJson(json);

  Map<String, dynamic> toJson() => _$LoginRequestToJson(this);

}

@JsonSerializable()
class LoginUserInfos {
  final String? nom;
  final String? prenom;
  final String? email;
  final String? login;

  LoginUserInfos({
    this.nom,
    this.prenom,
    this.email,
    this.login,
  });

  factory LoginUserInfos.fromJson(Map<String, dynamic> json) =>
      _$LoginUserInfosFromJson(json);

  Map<String, dynamic> toJson() => _$LoginUserInfosToJson(this);
}

@JsonSerializable()
class LoginToken {
  @JsonKey(name: "access_token")
  final String? accessToken;
  @JsonKey(name: "expire_in")
  final String? expireIn;
  @JsonKey(name: "token_type")
  final String? tokenType;

  LoginToken({
    this.accessToken,
    this.expireIn,
    this.tokenType,
  });

  factory LoginToken.fromJson(Map<String, dynamic> json) =>
      _$LoginTokenFromJson(json);

  Map<String, dynamic> toJson() => _$LoginTokenToJson(this);
}
