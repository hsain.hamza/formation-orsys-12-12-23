import 'package:my_awesome_app/app/modules/login/data/remote/models/login_models.dart';
import 'package:my_awesome_app/app/modules/login/data/remote/providers/api/login_rest_api_client.dart';
import 'package:my_awesome_app/core/http/http_client.dart';

abstract class LoginRemoteProvider {
  Future<LoginResponse> login(String email, String password);
}

class LoginRemoteProviderImpl extends LoginRemoteProvider {

  final  _loginRestApiClient = LoginRestApiClient(HttpClient.client);
  @override
  Future<LoginResponse> login(String email, String password) {
    return _loginRestApiClient.login(LoginRequest(email: email, password: password));
  }

}
