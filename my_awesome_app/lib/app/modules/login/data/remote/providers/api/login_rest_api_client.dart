import 'package:dio/dio.dart';
import 'package:my_awesome_app/app/modules/login/data/remote/models/login_models.dart';
import 'package:retrofit/retrofit.dart';

part 'login_rest_api_client.g.dart';

@RestApi()
abstract class LoginRestApiClient {

  factory LoginRestApiClient(Dio dio, {String baseUrl}) = _LoginRestApiClient;

  @POST("/login")
  Future<LoginResponse> login(@Body() LoginRequest data);
}