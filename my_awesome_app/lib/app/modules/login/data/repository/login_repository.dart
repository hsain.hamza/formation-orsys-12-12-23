import 'package:my_awesome_app/app/modules/login/data/local/providers/login_local_provider.dart';
import 'package:my_awesome_app/app/modules/login/data/remote/models/login_models.dart';
import 'package:my_awesome_app/app/modules/login/data/remote/providers/login_remote_provider.dart';
import 'package:my_awesome_app/core/di/locator.dart';

abstract class LoginRepository {
  Future<LoginResponse> getCurrentUser();
  Future<LoginResponse> login(String username, String password);
  Future<void> logout();
}

class LoginRepositoryImpl extends LoginRepository {
  final LoginRemoteProvider _loginRemoteProvider = injector.get();
  final LoginLocalProvider _loginLocalProvider = injector.get();

  @override
  Future<LoginResponse> getCurrentUser() async {
    await _loginLocalProvider.init();

    return await _loginLocalProvider.fetch();
  }

  @override
  Future<LoginResponse> login(String username, String password) async {
    final response = await _loginRemoteProvider.login(username, password);
    await _loginLocalProvider.init();
    await _loginLocalProvider.store(response);
    return response;
  }

  @override
  Future<void> logout() async {
    await _loginLocalProvider.init();

    return await _loginLocalProvider.delete();
  }

}