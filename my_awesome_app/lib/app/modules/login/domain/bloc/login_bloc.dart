import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:my_awesome_app/app/modules/login/data/remote/models/login_models.dart';
import 'package:my_awesome_app/app/modules/login/data/repository/login_repository.dart';
import 'package:my_awesome_app/core/di/locator.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final LoginRepository _loginRepository = injector.get();

  LoginBloc() : super(LoginInitial()) {
    on<CheckLogin>((event, emit) async {
      try{
        final currentUser = await _loginRepository.getCurrentUser();
        emit(Authenticated(loginResponse: currentUser));
      } catch(e){
        emit(Unauthenticated());
      }
    });

    on<LogoutUser>((event, emit) async {
      await _loginRepository.logout();
      emit(Unauthenticated());
    });

    on<LoginUser>((event, emit) async {
      try{
        final user = await _loginRepository.login(event.username, event.password);
        emit(Authenticated(loginResponse: user));
      } catch(e){
        emit(Unauthenticated());
      }
    });
  }
}
