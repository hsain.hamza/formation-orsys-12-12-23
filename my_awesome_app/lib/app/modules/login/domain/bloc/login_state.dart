part of 'login_bloc.dart';

@immutable
abstract class LoginState {}

class LoginInitial extends LoginState {}

class Authenticated extends LoginState {
  final LoginResponse loginResponse;

  Authenticated({required this.loginResponse});
}

class Unauthenticated extends LoginState {}

class AuthenticationInProgress extends LoginState {}


